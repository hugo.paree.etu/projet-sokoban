#include "main.h"

//Demande une entrée utilisateur, renvoie la premiere occurence d'un caratère accepté ou ' ' si aucun trouvé
char entree_du_joueur (void){
  char* entree = malloc(sizeof(char)*255);
  printf("\nappuyez sur a pour revenir en arrière\nou entrez une direction zqsd : ");
  scanf("%s",entree);
  for (int i = 0; i < 255; ++i){
    if(entree[i] != 'z' || entree[i] != 'q' || entree[i] != 'd' || entree[i] != 's' || entree[i] != 'a'){
      return entree[i];
    }
  }
  return ' ';
}

//verifie si on peut pousser la caisse et modifie le terrain si c'est possible, renvoie
// 1 si la caisse a été poussée
int pousser_caisse(niveau_t* niveau, int depcolonne, int depligne, char dep){
  int pousser = 0;
   switch(dep){
    case 'z':
      depligne--;
      break;
    case 's':
      depligne++;
      break;
    case 'q':
      depcolonne--;
      break;
    case 'd':
      depcolonne++;
      break;
  }
  switch(lecture_du_terrain(niveau, depcolonne, depligne)){
    case '.':
      place_sur_terrain(niveau, depcolonne, depligne, '*');
      pousser++;
      break;
    case ' ':
      place_sur_terrain(niveau, depcolonne, depligne, '$');
      pousser++;
      break;
  }
  return pousser;
}

//remet la case de la à ce quelle était sans l'objet dessus
void remettre_case(niveau_t* niveau, int colonne, int ligne){
  switch(lecture_du_terrain(niveau, colonne, ligne)){
    case '+':
      place_sur_terrain(niveau, colonne, ligne, '.');
      break;
    case '@':
      place_sur_terrain(niveau, colonne, ligne, ' ');
      break;
    case '*':
      place_sur_terrain(niveau, colonne, ligne, '.');
      break;
  }
}

//bouge le joueur en vérifiant le char a mettre selon ce qu'il se trouve sur la case
void placer_joueur(niveau_t* niveau, int depcolonne, int depligne){
  niveau->perso->colonne = depcolonne;
  niveau->perso->ligne = depligne;
  niveau->nb_de_pas++;
  switch(lecture_du_terrain(niveau, depcolonne, depligne)){
    case '.':
      place_sur_terrain(niveau, depcolonne, depligne, '+');
      break;
    case '*':
      place_sur_terrain(niveau, depcolonne, depligne, '+');
      break;
    default :
      place_sur_terrain(niveau,depcolonne, depligne, '@');
  }
}

//déplace le joueur dans la direction demandée si possible 
void deplacement (niveau_t* niveau, char direction){
  char dep = direction;
  int depligne = niveau->perso->ligne;
  int depcolonne = niveau->perso->colonne;

  //préparer le déplacement
  switch(dep){
    case 'z':
      depligne--;
      break;
    case 's':
      depligne++;
      break;
    case 'q':
      depcolonne--;
      break;
    case 'd':
      depcolonne++;
      break;
  }
  //Action selon la case vers laquelle on se déplace
  
  switch(lecture_du_terrain(niveau, depcolonne, depligne)){
    case '#':
      break;
    case '$':
      if(pousser_caisse(niveau, depcolonne, depligne, dep)==1){
        remettre_case(niveau, niveau->perso->colonne, niveau->perso->ligne);
        placer_joueur(niveau, depcolonne, depligne);
      }
      break;
    case '*':
      if(pousser_caisse(niveau, depcolonne, depligne, dep)==1){
        remettre_case(niveau, niveau->perso->colonne, niveau->perso->ligne);
        placer_joueur(niveau, depcolonne, depligne);
      }
      break;
    default:
      remettre_case(niveau, niveau->perso->colonne, niveau->perso->ligne);
      placer_joueur(niveau, depcolonne, depligne);
      break;
  }
}

//demande une entrée utilisateur et renvoie un int de l'entrée si elle est correct, -1 si non correct
int selection_niveau(void){
  char entree[255];
  printf("veuillez entrer un numero de niveau : ");
  scanf("%s", entree);
  printf("\n");
  for (int i = 0; i < 255; ++i){
    if((entree[i]<'0' && entree[i]>'9') || entree[i]=='\0') return atoi(entree);
  }
  
  return -1;
}

//demande le nom du joueur et le renvoie, maximum 255 caractères
char* nom_du_joueur (void){
  char* entree = malloc(sizeof(char)*255);
  printf("\nentrez votre nom de joueur : ");
  scanf("%s",entree);
  return entree;
}
