#include "main.h" // include le main.h je crois, faut rajouter des commentaires partout non ? ah zut je crois c'est pas dans ce projet que c'est demandé.

//efface tout sur l'écran
void clear(void){
	printf("\033[2J\033[H");
}

//affiche qu'aucun niveau à été trouvé
void aucun_niveau(){
	printf(ANSI_COLOR_RED "Aucun niveau trouvé.\n" ANSI_COLOR_RESET);
	sleep(1);
	clear();
}

//un temps de chargement factice car le jeu est trop rapide d'exécution
void fake_loading(){
	printf("\n");
	for (int i = 0; i < 100; ++i){
		printf("\033[1A\x1B[2K");// \033[1A bouge le curseur une ligne vers le haut \x1B[2K supprime la ligne ou se trouve le curseur
		printf("loading %d%%\n", i);
		usleep(10000);
	}
	printf("\033[1A\x1B[2K");
	printf(ANSI_COLOR_GREEN"loading 100%%\n" ANSI_COLOR_RESET);
	sleep(1);
	clear();
}

//fait les actions nécessaire pour que le joueur joue tant qu'il n'a pas gagné, oui vous devez finir le jeu pour sortir.
int play(niveau_t* niveau, historique_t* hist){
	char dep = ' ';
	while(niveau_termine(niveau)==0){ //tant que le niveau n'est pas fini
		clear();
		affichage_niveau(niveau);
		dep = entree_du_joueur();
		if(dep == 'a'){
			if(hist->taille>0)	niveau = coup_precedent(hist);
		} else {
			sauvegarde_un_coup (hist, niveau);
			deplacement(niveau, dep);
		}
	}
	clear();
	affichage_niveau(niveau);

	return niveau->nb_de_pas;
}

//regarde si le score du joueur est suffisament élevé pour entrer dans le top 5 des meilleurs scores
int merite_de_jouer(int score_joueur, int nbNiv){ // si le joueur est dans le top 5
	score_t** scores = lecture_des_scores(nbNiv);
	int nb = 0;
	while(nb<5 &&scores[nb]->init!=0){
		nb++;
	}
	sleep(1);
	if(nb!=5) return 1;
	if(scores[4]->score > score_joueur) return 1;

	return 0;
}

//ça c'est le main ya bcp de chose du genre demander quel niveau jouer puis ensuite jouer et entrer les scores
int main(void){
	niveau_t* niveau;
	int nbNiv;
	clear();
	do{
		nbNiv = selection_niveau();
		if(!(nbNiv == -1)){
			niveau = lecture_du_niveau(nbNiv);
			if(niveau==NULL){
				aucun_niveau();
				niveau = NULL;
			}
		}else{
			aucun_niveau();
		}
		
	}while(niveau==NULL);

	historique_t* hist = initialiser_historique();
	fake_loading(); //tout est fake de nos jours...
	int score_joueur = play(niveau, hist);
	printf(ANSI_COLOR_GREEN "\nVous avez gagné !\n" ANSI_COLOR_RESET);
	sleep(1);
	if(merite_de_jouer(score_joueur, nbNiv)==1){
		ecriture_des_scores(nbNiv, score_joueur, nom_du_joueur());
	}
	usleep(1000000);
	printf("\033[1A\x1B[2K");// \033[1A bouge le curseur une ligne vers le haut \x1B[2K supprime la ligne ou se trouve le curseur
	score_t** scores = lecture_des_scores(nbNiv);
	printf("top 5 scores du niveau :\n");
	int nb = 0;
	while(nb<5 && scores[nb]->init==1){
		nb++;
	}
	for (int i = 0; i < nb; ++i){
		printf("	%s %d\n",scores[i]->nom_joueur, scores[i]->score );
	}
	
	if(lecture_du_score(nbNiv)>score_joueur || lecture_du_score(nbNiv)==-1){
		ecriture_du_score (nbNiv, score_joueur);
	}
	liberation_historique(hist);
	liberation_du_niveau(niveau);
	sleep(2);
	clear();
	return 0;
}

