#include "main.h"

//renvoie une copie conforme du niveau_t passé en paramètre
niveau_t* copie_du_niveau (niveau_t* niveau){
  niveau_t* new_niveau = nouveau_niveau(niveau->nb_colonnes, niveau->nb_lignes);
  for (int i = 0; i < niveau->nb_colonnes*niveau->nb_lignes; ++i){
    new_niveau->terrain[i] = niveau->terrain[i];
  }
  new_niveau->nb_de_pas = niveau->nb_de_pas;
  new_niveau->perso = malloc(sizeof(point_t));
  new_niveau->perso->colonne = niveau->perso->colonne;
  new_niveau->perso->ligne = niveau->perso->ligne;
  return new_niveau;
}

//initialise et renvoie un historique_t
historique_t* initialiser_historique(){
  historique_t* historique = malloc(sizeof(historique_t));
  historique->taille = 0;
  historique->tableau = malloc(sizeof(niveau_t*)*120);
  for (int i = 0; i < 120; ++i){
    historique->tableau[i] = malloc(sizeof(niveau_t));
  }
  return historique;
}

//libère historique_t
void liberation_historique(historique_t* historique){
  for (int i = 0; i < historique->taille; ++i){
    liberation_du_niveau(historique->tableau[i]);
  }
  free(historique->tableau);
  free(historique);
}

//ajoute une copie du niveau_t dans l'historique
void sauvegarde_un_coup (historique_t* hist, niveau_t* niveau){
  hist->tableau[hist->taille] = copie_du_niveau(niveau);
  hist->taille = hist->taille + 1;
}

//retourne le coup précédent
niveau_t* coup_precedent (historique_t* hist){
  hist->taille = hist->taille-1;
  return hist->tableau[hist->taille];
}
