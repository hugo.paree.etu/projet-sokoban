#include "main.h"

//écrit le score passé en paramètre dans un fichier score_nomduniveau
void ecriture_du_score (int quel_niveau, int score){
	FILE* fichier;
	char filename[sizeof(char)*18];
	snprintf( filename, sizeof(filename), "%s%i", "Scores/score_", quel_niveau);
	fichier = fopen(filename, "w");
	char* scoreString = malloc(sizeof(char)*1024);
	sprintf(scoreString, "%d", score);
	fprintf(fichier, "%s", scoreString);
	fclose(fichier);
}

//lit le score contenu dans le fichier de sauvegarde du score du niveau
int lecture_du_score (int quel_niveau){
	FILE* fichier;
	char filename[sizeof(char)*18];
	snprintf( filename, sizeof(filename), "%s%i", "Scores/score_", quel_niveau);
	fichier = fopen(filename, "r");
	if (fichier == NULL){
		fichier = fopen(filename, "w");
		fprintf(fichier, "-1");
		fclose(fichier);
		fichier = fopen(filename, "r");
	}
	int score = -1;
	fscanf(fichier,"%d", &score);
	fclose(fichier);
	return score;
}

//écrit le top 5 des scores du niveau dans un fichier score_multi_nomniveau en les triant dans l'ordre
void ecriture_des_scores(int quel_niveau, int score , char* pseudo){
	score_t** scores = lecture_des_scores(quel_niveau);
	int nb = 0;
	int deja_vu = 1; //I've just been in this place before
	int position = 0; // position du doublon de pseudo
	if(scores[4]->init==1){
		if (scores[4]->score > score){
			scores[4]->score = score;
			scores[4]->nom_joueur = pseudo;
		}
		nb = 5;
	}else{
		while(scores[nb]->init==1 && nb<5){
			if(strcmp(scores[nb]->nom_joueur, pseudo)==0){
				position = nb;
				deja_vu = 0;
			}
			nb++;
		}
		if(deja_vu==1){
			scores[nb] = malloc(sizeof(score_t));
			scores[nb]->init = 1;
			scores[nb]->nom_joueur = pseudo;
			scores[nb]->score = score;
			nb++;
		}else{
			if(scores[position]->score > score) scores[position]->score = score;
		}
		
	}

	for (int i = 0; i < nb; ++i) {
  		int min = scores[i]->score;
  		char* pseudonyme = scores[i]->nom_joueur;
  		int minIndex = i;
  			// on commence après car les valeurs d'avant sont triées.
  		for (int j = i+1; j < nb; ++j) {
   			if (scores[j]->score < min) {
      			minIndex = j;
     			min = scores[j]->score;
     			pseudonyme = scores[j]->nom_joueur;
    		}
 		}
  		if (minIndex != i) {
    		scores[minIndex]->score = scores[i]->score; 
    		scores[minIndex]->nom_joueur = scores[i]->nom_joueur;
    		scores[i]->score = min;
    		scores[i]->nom_joueur = pseudonyme;
  		}
	}
	FILE* fichier;
	char filename[sizeof(char)*80];
	snprintf( filename, sizeof(filename), "%s%i", "Scores/score_multi_", quel_niveau);
	fichier = fopen(filename, "w");

	char* scoreString = malloc(sizeof(score_t)+sizeof(char)*80);
	for (int i = 0; i < nb; ++i){
		sprintf(scoreString,"%s %d\n", scores[i]->nom_joueur, scores[i]->score);
		fputs(scoreString, fichier);
	}
	fclose(fichier);
}

//lit le top 5 des scores et les renvoie sous forme d'une structure
score_t** lecture_des_scores(int quel_niveau){
	FILE* fichier;
	char filename[sizeof(char)*30];
	snprintf( filename, sizeof(filename), "%s%i", "Scores/score_multi_", quel_niveau);
	fichier = fopen(filename, "r");
	if (fichier == NULL){
		fichier = fopen(filename, "w");
		fclose(fichier);
		fichier = fopen(filename, "r");
	}
	score_t** scores = malloc(5*sizeof(score_t*));
	for (int i = 0; i < 5; ++i){
		scores[i] = malloc(sizeof(score_t));
		scores[i]->init = 0;
	}

	int idx = 0;
	char* buffer = malloc(sizeof(char)*80);
	while (fscanf(fichier, "%80[^\n]\n", buffer) != EOF && idx<5){
		scores[idx] = malloc(sizeof(score_t));
		scores[idx]->nom_joueur = malloc(80*sizeof(char));
		scores[idx]->init=1;
		sscanf(buffer, "%s %d", scores[idx]->nom_joueur, &scores[idx]->score);
		idx++;
    }
	fclose(fichier);
	return scores;
}