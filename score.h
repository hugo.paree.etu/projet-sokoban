typedef struct{
	int init;
	char* nom_joueur;
	int score;
}score_t;

int lecture_du_score (int quel_niveau);
void ecriture_du_score (int quel_niveau, int score);
void ecriture_des_scores(int quel_niveau, int score , char* pseudo);
score_t** lecture_des_scores(int quel_niveau);