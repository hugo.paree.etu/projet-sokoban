#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "terrain.h"
#include "input.h"
#include "score.h"
#include "historique.h"

#define ANSI_COLOR_BLACK   "\x1b[90m"
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[48;5;178m"
#define ANSI_COLOR_BLUE    "\x1b[94m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_BACKGROUND_WHITE "\x1b[107m"
#define ANSI_BACKGROUND_YELLOW "\x1b[103m"
#define ANSI_BACKGROUND_BLACK "\x1b[102m"

