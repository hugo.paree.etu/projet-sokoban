typedef struct{
  niveau_t** tableau;
  int taille;
}historique_t;

niveau_t* copie_du_niveau (niveau_t* niveau);
historique_t* initialiser_historique(void);
void liberation_historique(historique_t* historique);
void sauvegarde_un_coup (historique_t* hist, niveau_t* niveau);
niveau_t* coup_precedent (historique_t* hist);
