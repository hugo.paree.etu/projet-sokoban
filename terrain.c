#include "main.h"

//initialise et renvoie un niveau_t avec le nombre de colonnes et de lignes demandé
niveau_t* nouveau_niveau(int nb_colonnes, int nb_lignes){
  niveau_t* niveau = malloc(sizeof(niveau_t));
  char* newTerrain = malloc(nb_colonnes*nb_lignes*sizeof(char));
  niveau->terrain = newTerrain;
  niveau->nb_colonnes = nb_colonnes;
  niveau->nb_lignes = nb_lignes;

  return niveau;
}

//place le char <car> à la position colonne, ligne dans le terrain
void place_sur_terrain (niveau_t* niveau, int colonne, int ligne, char car){
	niveau->terrain[ligne * niveau->nb_colonnes + colonne] = car;
}

//c'est demandé dans le sujet mais useless
void initialise_le_terrain (niveau_t* niveau){
	for (int i = 0; i < (niveau->nb_colonnes)*(niveau->nb_lignes); ++i){
		niveau->terrain[i] = '#';
	}
}

//renvoie le char à la position colonne, ligne du terrain
char lecture_du_terrain (niveau_t* niveau, int colonne, int ligne){
  return niveau->terrain[ligne*niveau->nb_colonnes+colonne];
}

//affiche le niveau avec des couleurs et des symboles custom
void affichage_niveau (niveau_t* niveau){
	printf("\n" ANSI_BACKGROUND_WHITE);
	for (int ligne = 0; ligne < niveau->nb_lignes; ++ligne){
		for (int colonne = 0; colonne < niveau->nb_colonnes; ++colonne){
			switch (lecture_du_terrain(niveau, colonne, ligne)){
				case '@': 
					printf(ANSI_COLOR_BLUE "◊" );
					break;
				case ' ':
					printf(" ");
					break;
				case '#':
					printf(ANSI_BACKGROUND_YELLOW "" ANSI_COLOR_RED "▒" ANSI_COLOR_RESET "" ANSI_BACKGROUND_WHITE);
					break;
				case '.':
					printf(ANSI_COLOR_MAGENTA "·");
					break;
				case '$':
					printf(ANSI_COLOR_RED "▢");
					break;
				case '*':
					printf(ANSI_COLOR_GREEN "✔");
					break;
				case '+': 
					printf(ANSI_COLOR_BLUE "✘");
					break;
				default: break;
			}
		}
		printf( "\n" );
	}
	printf("" ANSI_COLOR_RESET);
	printf("score : %d\n", niveau->nb_de_pas);

}

//Lit le niveau depuis un fichier
niveau_t* lecture_du_niveau(int quel_niveau){
	FILE* fichier;
	char filename[sizeof(char)*18];
	snprintf( filename, sizeof(filename), "%s%i", "Niveaux/niveau_", quel_niveau);
	fichier = fopen(filename, "r");
	if (fichier == NULL){
		return NULL;
	}
	int colonne = 0; int ligne = 0;
	fscanf(fichier, "%d %d", &colonne, &ligne);
	niveau_t* niveau = nouveau_niveau(colonne,ligne);
	int lig = 0;int col = 0;
	int car = fgetc(fichier);
	while(car=='\n'){
		car = fgetc(fichier);
	}
	while(car != EOF && lig < ligne){
		while(col < colonne){
			if(car!='\n' && car!=EOF){
				place_sur_terrain(niveau, col, lig, car);
				car = fgetc(fichier);
			}else{
				place_sur_terrain(niveau, col, lig, ' ');				
			}
			col++;
		}
		car = fgetc(fichier);
		col = 0;
		lig++;
	}

	fclose(fichier);
	// récupération position du personnage
	point_t* perso = malloc(sizeof(perso));

	for (int ligne = 0; ligne < niveau->nb_lignes; ++ligne){
		for (int colonne = 0; colonne < niveau->nb_colonnes; ++colonne){
			switch (lecture_du_terrain(niveau, colonne, ligne)){
				case '@': 
					perso->colonne = colonne;
					perso->ligne = ligne;
					break;
			}
		}
	}
	niveau->perso = perso;
	niveau->nb_de_pas = 0;
	return niveau;
}

//libère un niveau_t
void liberation_du_niveau(niveau_t* niveau){
	free(niveau->terrain);
	free(niveau->perso);
	free(niveau);
}

//return 1 si le joueur a fini le niveau càd qu'il n'y a plus aucun $
int niveau_termine (niveau_t* niveau){
	int termine = 1;
	for (int ligne = 0; ligne < niveau->nb_lignes; ++ligne){
		for (int colonne = 0; colonne < niveau->nb_colonnes; ++colonne){
			if(lecture_du_terrain(niveau, colonne, ligne)=='$') termine = 0;
		}
	}
	return termine;
}