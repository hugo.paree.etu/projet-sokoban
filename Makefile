CC = gcc
CFLAGS = -Wall -Wextra -c

sokoban : main.o terrain.o input.o score.o historique.o | Scores Niveaux
	$(CC) -o $@ $^

main.o : main.c main.h
	$(CC) $(CFLAGS) $<

terrain.o : terrain.c
	$(CC) $(CFLAGS) $<

input.o : input.c
	$(CC) $(CFLAGS) $<

score.o : score.c
	$(CC) $(CFLAGS) $<

historique.o : historique.c
	$(CC) $(CFLAGS) $<

Scores :
	mkdir -p $@	

Niveaux :
	mkdir -p $@	

clean :
	rm -rf sokoban *.o *~ Scores